import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HeroesService } from '../../services/heroes.service';
import { Heroe } from '../interfaces/heroes.interface';

@Component({
  selector: 'app-heroe',
  templateUrl: './heroe.component.html',
  styles: [`
    img {
      width: 100%;
      border-radius: 5px;
    }
  `]
})
export class HeroeComponent implements OnInit {

  heroe!:Heroe;
  id:string='';

  constructor(private router: Router, private activatedRoute: ActivatedRoute, private heroeService:HeroesService) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe((params) => {this.id=params.id });
    this.heroeService.getHeroePorId(this.id).subscribe(heroe=>{
     console.log(heroe);
     this.heroe= heroe;
    })
  }
  regresar(){
    this.router.navigate(['/heroes/listado'])
  }

}
